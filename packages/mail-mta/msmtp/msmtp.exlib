# Copyright (c) 2008-2009 Vöröskői András <voroskoi@gmail.com>
# Copyright (c) 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

export_exlib_phases src_configure src_install

SUMMARY="msmtp is an smtp client"
DESCRIPTION="
In the default mode, it transmits a mail to an SMTP server (for example at a free mail provider) which does the delivery.  To use this program with your mail user agent (MUA), create a configuration file with your mail account(s) and tell your MUA to call msmtp  instead of /usr/sbin/sendmail.
"
HOMEPAGE="https://marlam.de/msmtp/"
DOWNLOADS="https://marlam.de/${PN}/releases/${PNV}.tar.xz"

UPSTREAM_DOCUMENTATION="https://marlam.de/msmtp/documentation/"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="idn vim-syntax
    gsasl   [[ description = [ SASL authentication support ] ]]
    keyring [[ description = [ Use gnome-keyring for password storage ] ]]
    msmtpd [[ description = [ Minimal SMTP server ] ]]
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        sys-devel/gettext
    build+run:
        gsasl? ( net-libs/gsasl )
        idn? ( net-dns/libidn2:= )
        keyring? ( dev-libs/libsecret:= )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        providers:gnutls? ( dev-libs/gnutls[>=3.4.0] )
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( doc/msmtprc-system.example doc/msmtprc-user.example )

msmtp_src_configure() {
    econf --enable-nls \
          $(if option providers:libressl; then
                echo --with-tls=libtls
            elif option providers:openssl; then
                echo --with-tls=openssl
            else
                echo --with-tls=gnutls
            fi
          ) \
          $(option_with gsasl libgsasl) \
          $(option_with idn libidn) \
          $(option_with keyring libsecret)
          $(option_with msmtpd)
}

msmtp_src_install() {
    default

    alternatives_for mta ${PN} 10 /usr/$(exhost --target)/bin/sendmail ${PN}

    if option vim-syntax ; then
        insinto /usr/share/vim/vimfiles/syntax
        doins scripts/vim/msmtp.vim
        insinto /usr/share/vim/vimfiles/ftdetect
        newins "${FILES}"/msmtp.vim msmtp.vim
    fi
}

